" mappings

"   --- Made it easy to learn vim
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>

"   --- Commenting/Uncommenting line by line
" no ¤¤ 0x<esc>j
" no ¤# 0i#<esc>j
" no ¤" 0i"<esc>j
" no ¤! 0i!<esc>j
" ino ¤¤ <esc>0x<esc>j
" ino ¤# <esc>0i#<esc>j
" ino ¤" <esc>0i"<esc>j
" ino ¤! <esc>0i!<esc>j

"   --- Some shorts I use all the time
ino jj <esc>
ino JJ <esc>A

"   --- Jumping buffers
nnoremap <Tab> :bnext<CR>
" nnoremap <C-H> :bprevious<CR>
nnoremap <S-Right> :bnext<CR>
nnoremap <S-Left> :bprevious<CR>

"   --- Clear search highlight
nnoremap <C-L> :nohl<CR><C-L>

"   --- Vifm plugin
"map <Leader>vv  :Vifm<CR>
"map <Leader>vs  :VsplitVifm<CR>
"map <Leader>sp  :SplitVifm<CR>
"map <Leader>tv  :TabVifm<CR>

"   --- autoindent on/off
map <leader>i   :setlocal autoindent<CR>
map <leader>I   :setlocal noautoindent<CR>

"   --- center document when entering instert mode
" utocmd InsertEnter * norm zz

"   --- shortcuts for split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
" map <C-l> <C-w>l

"   --- shortcuts for split opening and closing a split
nnoremap <leader>h :split<CR>
nnoremap <leader>s :vsplit<CR>
" deletes split
nnoremap <leader>d :only<CR>


