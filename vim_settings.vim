" set stuff

set nocompatible
set encoding=utf-8

" Make it possible to <Tab> thru buffers without save changes
set hidden

" Tab-completion of commands in vim
set wildmenu

" Shows last used command at cmd-line
set showcmd

" Find matching bracket, jump with %
set showmatch

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase
set path+=**

" Cleans up hassle if accidentally hitting something, don't rember what
set backspace=indent,eol,start

" Linenumbering, red line at the 79 length short
set number
set relativenumber
set cursorline
" set cursorculomn
" set colorcolumn=79


" Tab
set shiftwidth=4
set softtabstop=4
set expandtab

" autoindent on/off: i/I
set autoindent


" Put cursor at first blank, start of line, not at at set point
set nostartofline

" Not needed b/c statusline
"set ruler

" Enables mouse at any modes:  https://vim.fandom.com/wiki/Example_vimrc
set mouse=a

"   --- Statusline
set laststatus=2

set statusline=
set statusline+=%#DiffDelete#
set statusline+=\[%n]
set statusline+=\ %F
set statusline+=\ %r
set statusline+=%=
set statusline+=\ %y
set statusline+=\ <%{&fileencoding?&fileencoding:&encoding}>
set statusline+=%#SpellCap#
set statusline+=\[%5l]/
set statusline+=\[%5c]
set statusline+=\ %5L
set statusline+=\ %3p%%
set statusline+=\ -
"   --- /http://vimdoc.sourceforge.net/htmldoc/options.html#'statusline
