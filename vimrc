" vimrc loads from .config/vim
" minor change to try git.

"let loaded_netrwPlugin = 1
let did_load_ftplugin = 1
" if problem, deactivate by removing comment

set nocompatible
filetype plugin indent on
syntax on

so $HOME/.config/vim/vim_settings.vim
so $HOME/.config/vim/vim_mappings.vim

set laststatus=2

set statusline=
set statusline+=%#DiffDelete#
set statusline+=\[%n]
set statusline+=\ %F
set statusline+=\ %r
set statusline+=%=
set statusline+=\ %y
set statusline+=\ <%{&fileencoding?&fileencoding:&encoding}>
set statusline+=%#SpellCap#
set statusline+=\[%5l]/
set statusline+=\[%5c]
set statusline+=\ %5L
set statusline+=\ %3p%%
set statusline+=\ -
" /http://vimdoc.sourceforge.net/htmldoc/options.html#'statusline

colo blue

if &diff
  colo torte
endif
